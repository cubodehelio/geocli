'use strict';

module.exports = {
  babelrc: false,
  ignore: ['node_modules'],
  presets: [[require.resolve('babel-preset-env'), {
    targets: {
      node: 'current'
    }
  }]],
  plugins: []
};