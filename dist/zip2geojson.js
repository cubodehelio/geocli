'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _path = require('path');

var _zip2GeojsonStream = require('./zip2GeojsonStream');

var _zip2GeojsonStream2 = _interopRequireDefault(_zip2GeojsonStream);

var _analytics = require('./streams/analytics');

var analytics = _interopRequireWildcard(_analytics);

var _utils = require('./streams/utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (config) {

  var zipSrc = (0, _path.resolve)(process.cwd(), config.src);

  var destStream = void 0;

  if (config.dest) {
    destStream = (0, _fs.createWriteStream)((0, _path.resolve)(process.cwd(), config.dest));
  } else {
    destStream = process.stdout;
  }

  return (0, _zip2GeojsonStream2.default)(zipSrc).then(function (readStream) {

    var stream = readStream;

    if (config.countUniqueValues) {

      stream = stream.pipe(analytics.countUniqueValues(config.countUniqueValues)).pipe(utils.toJSON(config.pretty));
    } else if (config.autouid) {

      stream = stream.pipe(utils.addUid()).pipe(utils.toJSON(config.pretty)).pipe(utils.toFeatureCollection());
    } else if (config.collectInvalid) {

      console.error('--collect-invalid TODO!');
      process.exit(1);

      stream = stream.pipe(utils.collectInvalid()).pipe(utils.toJSON(config.pretty));
    } else {

      stream = stream.pipe(utils.toFeatureCollection());
    }

    stream.pipe(destStream);

    return new Promise(function (res, reject) {

      stream.on('error', reject);
      stream.on('close', function () {

        if (config.dest) {
          console.log('stream close');
        }

        return res();
      });
    });
  });
}; /* eslint global-require: 0 */