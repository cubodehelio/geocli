#!/usr/bin/env node
'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _zip2geojson = require('./zip2geojson');

var _zip2geojson2 = _interopRequireDefault(_zip2geojson);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Module dependencies.
 */

var srcPath = void 0,
    destPath = void 0;

_commander2.default.version(require('../package.json').version).option('-u, --count-unique-values <dottedPath>').option('-p, --pretty [boolean]').option('-i, --collect-invalid [boolean]').option('-b, --tobbox [boolean]').option('-d, --autouid [boolean]', 'generate and add a UID in every geojson feature').usage(`

  $ ziptogeo <src>               # Output go directly to stdout.
  $ ziptogeo <src> [dest]        # destination json file path (optional).
  $ ziptogeo <src> > dest.json   # pipe the output to a file.
  `).arguments('<src> [dest]').action(function (src, dest) {
  srcPath = src;
  destPath = dest;
}).parse(process.argv);

if (!srcPath) {

  _commander2.default.help();
} else {

  var cfg = {
    src: srcPath,
    dest: destPath,
    countUniqueValues: _commander2.default.countUniqueValues,
    pretty: _commander2.default.pretty,
    collectInvalid: _commander2.default.collectInvalid,
    tobbox: _commander2.default.tobbox,
    autouid: _commander2.default.autouid
  };

  (0, _zip2geojson2.default)(cfg).then(function () {
    process.exit();
  }).catch(function (err) {
    console.error(err.stack);
    process.exit(1);
  });
}