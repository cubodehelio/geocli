'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stream = require('stream');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ToFeatureCollectionStream = function (_Transform) {
  _inherits(ToFeatureCollectionStream, _Transform);

  function ToFeatureCollectionStream() {
    _classCallCheck(this, ToFeatureCollectionStream);

    var _this = _possibleConstructorReturn(this, (ToFeatureCollectionStream.__proto__ || Object.getPrototypeOf(ToFeatureCollectionStream)).call(this, { objectMode: true }));

    _this.startString = '{ "type":"FeatureCollection","features":[';
    _this.endString = ']}';
    _this.started = false;
    return _this;
  }

  _createClass(ToFeatureCollectionStream, [{
    key: '_transform',
    value: function _transform(feature, enc, next) {

      var str = this.started ? ','.concat(JSON.stringify(feature)) : this.startString.concat(JSON.stringify(feature));

      this.started = true;

      this.push(str);
      next();
    }
  }, {
    key: '_flush',
    value: function _flush(done) {
      this.push(this.endString);
      done();
    }
  }]);

  return ToFeatureCollectionStream;
}(_stream.Transform);

exports.default = ToFeatureCollectionStream;