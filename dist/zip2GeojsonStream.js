'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _unzipper = require('unzipper');

var _stream = require('stream');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @param {String} zipfilePath - path to the zip file
 * @return {Promise} - resolves to central directory
 * information object with methods to extract individual
 * files.
 */
var filterSources = function filterSources(sources) {
  return sources.files.filter(function (info) {
    return info.path.match(/\.(dbf|shp)$/);
  });
};
var getShpSources = function getShpSources(sources) {
  return sources.filter(function (source) {
    return source.path.match(/\.(shp)$/);
  });
};

var shapefile = require('shapefile'); // undefined when use `import`

var ShapeReadStream = function (_Readable) {
  _inherits(ShapeReadStream, _Readable);

  function ShapeReadStream(shpStream, dbfStream) {
    _classCallCheck(this, ShapeReadStream);

    var _this = _possibleConstructorReturn(this, (ShapeReadStream.__proto__ || Object.getPrototypeOf(ShapeReadStream)).call(this, { objectMode: true }));

    _this.shpStream = shpStream;
    _this.dbfStream = dbfStream;
    _this.source = null;
    return _this;
  }

  _createClass(ShapeReadStream, [{
    key: 'readStream',
    value: function () {
      var _ref = _asyncToGenerator(_regenerator2.default.mark(function _callee() {
        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (this.source) {
                  _context.next = 4;
                  break;
                }

                _context.next = 3;
                return shapefile.open(this.shpStream, this.dbfStream, {
                  encoding: 'utf-8'
                });

              case 3:
                this.source = _context.sent;

              case 4:
                _context.next = 6;
                return this.source.read();

              case 6:
                return _context.abrupt('return', _context.sent);

              case 7:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function readStream() {
        return _ref.apply(this, arguments);
      }

      return readStream;
    }()
  }, {
    key: '_read',
    value: function _read() {
      var _this2 = this;

      this.readStream().then(function (result) {
        _this2.push(result.done ? null : result.value);
      });
    }
  }]);

  return ShapeReadStream;
}(_stream.Readable);

exports.default = function (path, options) {
  return _unzipper.Open.file(path).then(function (sources) {

    var shpAndDbfInfo = filterSources(sources);

    var shpSources = getShpSources(shpAndDbfInfo);

    if (!shpSources.length) {
      throw new Error(`The zip file ${path} does not contains any .shp file`);
    } else if (shpSources.length > 1) {
      throw new Error(`The zip file ${path} contains more than ONE .shp file`);
    }

    var shpPath = shpSources[0].path;
    var dbfSource = shpAndDbfInfo.find(function (source) {
      return source.path === shpPath.replace(/\.(shp)/, '.dbf');
    });

    if (!dbfSource) {
      throw new Error(`the passed zip file ${path} does not contains any ".dbf" file`);
    }

    var shpStream = shpSources[0].stream;
    var dbfStream = dbfSource.stream;

    return new ShapeReadStream(shpStream(), dbfStream());
  });
};