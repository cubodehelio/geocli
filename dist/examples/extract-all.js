'use strict';

var readdirSync = require('fs').readdirSync;
var execSync = require('child_process').execSync;

/**
 * run this example: `node extract-all.js`
 */

var src = '/home/jcanabate/geo/downloads/drive/intervenciones-v3/';

readdirSync(src).filter(function (file) {
  return file.match(/\.zip$/gi);
}).forEach(function (file) {

  var filename = src.concat(file);
  var newFilename = src.concat(file.replace('.zip', '.geojson'));

  execSync(`zip2geojson ${filename} ${newFilename} -d`);

  console.log('done:', file);
});

console.log('all done');