'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _path = require('path');

var _utils = require('./streams/utils');

var utils = _interopRequireWildcard(_utils);

var _geojsonStream = require('geojson-stream');

var _geojsonStream2 = _interopRequireDefault(_geojsonStream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

exports.default = function (config) {
  return new Promise(function (res, reject) {

    var jsonSrc = (0, _path.resolve)(process.cwd(), config.src);

    var destStream = void 0;

    if (config.dest) {
      destStream = (0, _fs.createWriteStream)((0, _path.resolve)(process.cwd(), config.dest));
    } else {
      destStream = process.stdout;
    }

    var readable = (0, _fs.createReadStream)(jsonSrc);

    var pipe = readable.pipe(_geojsonStream2.default.parse());

    if (config.autouid) {
      pipe = pipe.pipe(utils.addUid());
    }

    if (config.removeProperties && config.removeProperties.length) {
      pipe = pipe.pipe(utils.removeProperties(config.removeProperties));
    }

    if (config.addBbox) {
      pipe = pipe.pipe(utils.addBbox());
    }

    /**
     * basically we need to handle two possible pipes ona that at the end leads
     * in a plain object that need to be converted to json string
     * and other that is already a json string.
     */

    if (config.toBbox) {
      pipe = pipe.pipe(utils.toBbox()).pipe(utils.toJSONArray(config.pretty));
    } else {
      pipe = pipe.pipe(utils.toFeatureCollection());
    }

    pipe = pipe.pipe(destStream);

    pipe.on('error', function (err) {
      // console.error('stream error...')
      reject(err);
    });
    pipe.on('close', function () {

      if (config.dest) {
        console.log('stream close');
      }

      return res();
    });
  });
};