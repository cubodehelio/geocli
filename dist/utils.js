'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Covert a string like:
 *
 * ```
 * "-68.09599718832222,-38.94101958422372 -68.09567643840339,-38.94061805516469 ..."
 * ```
 * to an array like:
 *
 * ```js
 * [
 *   [ -68.09599718832222, -38.94101958422372 ],
 *   [ -68.09567643840339, -38.94061805516469 ],
 *   [ ... ]
 * ]
 * ```
 *
 * @param {String} str
 * @return {Array<Array>} array of tuples of float values
 */
var parsePolygonString = exports.parsePolygonString = function parsePolygonString(str) {
  return str.split(' ').map(function (pair) {
    return pair.split(',').map(function (coordString) {
      return parseFloat(coordString);
    });
  });
};