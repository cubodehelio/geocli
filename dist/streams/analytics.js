'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.countUniqueValues = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stream = require('stream');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function getValueInPath(dottedPath, obj) {
  var pathArray = Array.isArray(dottedPath) ? dottedPath : dottedPath.split('.');

  return pathArray.reduce(function (acc, pathItem) {
    return acc[pathItem];
  }, obj);
}

var CountUniqueValues = function (_Transform) {
  _inherits(CountUniqueValues, _Transform);

  function CountUniqueValues(dottedPath) {
    _classCallCheck(this, CountUniqueValues);

    var _this = _possibleConstructorReturn(this, (CountUniqueValues.__proto__ || Object.getPrototypeOf(CountUniqueValues)).call(this, { objectMode: true }));

    _this.uniqueKeys = {};
    _this.path = dottedPath.split('.');
    return _this;
  }

  _createClass(CountUniqueValues, [{
    key: '_transform',
    value: function _transform(feature, enc, next) {

      try {

        var value = getValueInPath(this.path, feature);

        if (!this.uniqueKeys[value]) {
          this.uniqueKeys[value] = 1;
        } else {
          this.uniqueKeys[value]++;
        }
      } catch (err) {
        console.warn('--------------------------------------------------------------------');
        console.warn(feature);
        console.warn(err.stack);
      }

      next();
    }
  }, {
    key: '_flush',
    value: function _flush(done) {
      this.push(this.uniqueKeys);
      done();
    }
  }]);

  return CountUniqueValues;
}(_stream.Transform);

/**
 * @param {string} dottedPath - come from `--count-unique-values`
 */


var countUniqueValues = exports.countUniqueValues = function countUniqueValues(dottedPath) {
  return new CountUniqueValues(dottedPath);
};