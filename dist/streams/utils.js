'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toFeature = exports.parseEmbeddedJson = exports.renameKeys = exports.jsonStringToJs = exports.firstLineAndExit = exports.addUid = exports.removeProperties = exports.toDebug = exports.toJSONArray = exports.toBbox = exports.addBbox = exports.collectInvalid = exports.toFeatureCollection = exports.toJSON = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _stream = require('stream');

var _geojsonhint = require('@mapbox/geojsonhint');

var geojsonhint = _interopRequireWildcard(_geojsonhint);

var _bbox = require('@turf/bbox');

var _bbox2 = _interopRequireDefault(_bbox);

var _uidSafe = require('uid-safe');

var _uidSafe2 = _interopRequireDefault(_uidSafe);

var _utils = require('../utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var jsonify = function jsonify(obj, pretty) {
  return pretty ? JSON.stringify(obj, null, 2) : JSON.stringify(obj);
};

var ToFeatureCollectionStream = function (_Transform) {
  _inherits(ToFeatureCollectionStream, _Transform);

  function ToFeatureCollectionStream() {
    _classCallCheck(this, ToFeatureCollectionStream);

    var _this = _possibleConstructorReturn(this, (ToFeatureCollectionStream.__proto__ || Object.getPrototypeOf(ToFeatureCollectionStream)).call(this, { objectMode: true }));

    _this.startString = '{ "type":"FeatureCollection","features":[';
    _this.endString = ']}';
    _this.started = false;
    return _this;
  }

  _createClass(ToFeatureCollectionStream, [{
    key: '_transform',
    value: function _transform(feature, enc, next) {

      var featureStr = typeof feature === 'string' ? feature : JSON.stringify(feature);
      var str = this.started ? ','.concat(featureStr) : this.startString.concat(featureStr);

      this.started = true;
      this.push(str);
      next();
    }
  }, {
    key: '_flush',
    value: function _flush(done) {
      this.push(this.endString);
      done();
    }
  }]);

  return ToFeatureCollectionStream;
}(_stream.Transform);

var CollectInvalid = function (_Transform2) {
  _inherits(CollectInvalid, _Transform2);

  function CollectInvalid() {
    _classCallCheck(this, CollectInvalid);

    return _possibleConstructorReturn(this, (CollectInvalid.__proto__ || Object.getPrototypeOf(CollectInvalid)).call(this, { objectMode: true }));
  }

  _createClass(CollectInvalid, [{
    key: '_transform',
    value: function _transform(feature, enc, next) {

      var errors = geojsonhint.hint(feature);

      if (errors.length) {
        console.error(errors);
        process.exit();
      }

      next();
    }
  }, {
    key: '_flush',
    value: function _flush(done) {
      done();
    }
  }]);

  return CollectInvalid;
}(_stream.Transform);

/**
 * JSON stringify js
 * @param {Boolean} pretty - prettify the output
 */


var toJSON = exports.toJSON = function toJSON() {
  var pretty = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  return new _stream.Transform({
    objectMode: true,
    transform(obj, enc, next) {

      var json = jsonify(obj, pretty);

      this.push(json);
      next();
    }
  });
};

var toFeatureCollection = exports.toFeatureCollection = function toFeatureCollection() {
  return new ToFeatureCollectionStream();
};

var collectInvalid = exports.collectInvalid = function collectInvalid() {
  return new CollectInvalid();
};

var addBbox = exports.addBbox = function addBbox() {
  return new _stream.Transform({
    objectMode: true,
    transform(feature, enc, next) {

      feature.geometry.bbox = (0, _bbox2.default)(feature);
      this.push(feature);
      next();
    }
  });
};

/**
 * receives a "geo-js" feature, calculates the bbox and return an
 * non-geojson object containing only the original properties plus
 * the bbox property
 */
var toBbox = exports.toBbox = function toBbox() {
  return new _stream.Transform({
    objectMode: true,
    transform(feature, enc, next) {
      feature.properties.bbox = (0, _bbox2.default)(feature);
      this.push(feature.properties);
      next();
    }
  });
};

var toJSONArray = exports.toJSONArray = function toJSONArray() {
  var pretty = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;


  var startString = '[';
  var endString = ']';

  var started = false;

  return new _stream.Transform({
    objectMode: true,
    transform(item, enc, next) {

      var jsonItem = typeof item === 'string' ? item : jsonify(item, pretty);
      var str = started ? ','.concat(jsonItem) : startString.concat(jsonItem);

      started = true;
      this.push(str);
      next();
    },
    flush(done) {
      this.push(endString);
      done();
    }
  });
};

var toDebug = exports.toDebug = new _stream.Transform({
  transform(item, enc, next) {
    var foo = void 0;

    if (Buffer.isBuffer(item)) {
      foo = item.toString();
    }

    console.log(foo);
    process.exit();
    next();
  }
});

var removeProperties = exports.removeProperties = function removeProperties() {
  var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  return new _stream.Transform({
    objectMode: true,
    transform(obj, enc, next) {

      props.forEach(function (key) {
        delete obj.properties[key];
      });

      this.push(obj);
      next();
    }
  });
};

var addUid = exports.addUid = function addUid() {
  return new _stream.Transform({
    objectMode: true,
    transform(feature, enc, next) {

      feature.properties.uid = _uidSafe2.default.sync(18);
      this.push(feature);
      next();
    }
  });
};

/**
 * show the first line of a string and exit
 */
var firstLineAndExit = exports.firstLineAndExit = function firstLineAndExit() {
  return new _stream.Transform({
    objectMode: false,
    transform(chunk, enc, next) {

      var str = chunk.toString();
      var linesArray = str.split('\n')[0].replace(/"/g, '').split(',');

      console.log(linesArray);
      process.exit();
    }
  });
};

/**
 * in JSON str
 * out JS
 */
var jsonStringToJs = exports.jsonStringToJs = new _stream.Transform({
  objectMode: true,
  transform(chunk, enc, next) {

    var json = chunk.toString();
    var js = JSON.parse(json);

    this.push(js);
    next();
  }
});

/**
 * in: JS
 * out: JS
 *
 * @param {*} renames
 */
var renameKeys = exports.renameKeys = function renameKeys(renames) {
  return new _stream.Transform({
    objectMode: true,
    transform(obj, enc, next) {

      var newObj = Object.keys(obj).reduce(function (acc, originalKey) {

        if (renames[originalKey]) {
          acc[renames[originalKey]] = obj[originalKey];
        } else {
          acc[originalKey] = obj[originalKey];
        }

        return acc;
      }, {});

      this.push(newObj);
      next();
    }
  });
};

var parseEmbeddedJson = exports.parseEmbeddedJson = function parseEmbeddedJson(keyName) {
  return new _stream.Transform({
    objectMode: true,
    transform(obj, enc, next) {

      if (typeof obj[keyName] !== 'undefined') {
        obj[keyName] = JSON.parse(obj[keyName]);
      } else {
        console.warn(`Warning: key name "${keyName}" is not defined`);
      }

      this.push(obj);
      next();
    }
  });
};

/**
 * convert a regular object having a "geometry" key into
 * a geojson feature object
 *
 * IN: JS
 */
var toFeature = exports.toFeature = new _stream.Transform({
  objectMode: true,
  transform(obj, enc, next) {

    var feature = {
      type: 'Feature',
      geometry: {},
      properties: {}
    };

    if (!obj.geometry) {

      console.warn('Warning: item does not have a "geometry" key');
    } else if (typeof obj.geometry === 'object') {

      feature.properties = obj;
      feature.geometry = Object.assign({}, obj.geometry);
      delete obj.geometry;
    } else if (typeof obj.geometry === 'string') {

      var polygon = (0, _utils.parsePolygonString)(obj.geometry);

      feature.geometry = {
        type: 'polygon',
        coordinates: [polygon]
      };

      delete obj.geometry;
      feature.properties = obj;
    } else {
      console.warn('Error: `geometry` property does not have the proper format:');
      console.warn();
      console.warn(obj.geometry);
      process.exit();
    }

    this.push(feature);
    next();
  }
});