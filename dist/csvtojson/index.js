'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _path = require('path');

var _csvtojson = require('csvtojson');

var _csvtojson2 = _interopRequireDefault(_csvtojson);

var _utils = require('../streams/utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (config) {
  return new Promise(function (res, reject) {

    var destinationStream = void 0;

    if (config.dest) {
      destinationStream = (0, _fs.createWriteStream)((0, _path.resolve)(process.cwd(), config.dest));
    } else {
      destinationStream = process.stdout;
    }

    /**
     * create the readable stream
     */

    var csvReadableSrc = (0, _fs.createReadStream)(config.src);

    csvReadableSrc.on('error', function (err) {

      if (err.message.match(/ENOENT/g)) {
        reject(err);
      }

      console.warn(err);
    });

    if (config.showColumns) {

      // this will exit after console.log the first line
      csvReadableSrc.pipe(utils.firstLineAndExit());
    }

    var stream = (0, _csvtojson2.default)().fromStream(csvReadableSrc).pipe(utils.jsonStringToJs);

    if (config.renameColumns) {
      stream = stream.pipe(utils.renameKeys(config.renameColumns));
    }

    if (config.parseEmbeddedJson) {
      stream = stream.pipe(utils.parseEmbeddedJson(config.parseEmbeddedJson));
    }

    if (config.toFeature) {
      stream = stream.pipe(utils.toFeature);
    }

    /**
     * convert to json
     */
    stream = stream.pipe(utils.toJSON(config.pretty));

    if (config.toFeatureCollection) {
      stream = stream.pipe(utils.toFeatureCollection());
    } else {
      stream = stream.pipe(utils.toJSONArray(config.pretty));
    }

    stream.pipe(destinationStream);
  });
};