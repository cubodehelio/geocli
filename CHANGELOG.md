2017-08-16
==========

+ fix wrong geojson conversion in `utils.toFeature` stream

2017-08-15
==========

## Features

+ improve `--show-columns` output of `csvtogeo`

## Devlog

+ add `yarn.lock` file
+ refactor `csvtogeo`

2017-08-10
==========

## Features

+ fix `csvtojson` not properly parsing geojson when coordinates
comes in a normal string instead of an embedded geojson.
+ Add an extra example of `csvtojson` usage in the readme.

2017-07-04
==========

## Devlog

+ add `--autouid` to the ignore patters in `geojson-process`
+ update npm `build` task to remove dist folder first
+ re-build the source

2017-06-29
==========

## Features

+ add a full featured `csvtogeo` cli tool
+ rename `zip2geojson` to `ziptogeo`
+ rename `geojson-process` to `geotogeo`

## Devlog

+ create util `firstLineAndExit` stream
+ create util `jsonStringToJs` stream
+ create util `renameKeys` stream
+ create util `parseEmbeddedJson` stream
+ create util `toFeature` stream


2017-06-01
==========

## Features

add `--autouid` option to `geojson-process`
add `--autouid` option to auto generate a uid per every feature
add `extract-all.js` example
add `--autouid` documentation in README.md
add `npm run build` script
add `dist/` to the repo
