import { Transform } from 'stream';

function getValueInPath(dottedPath, obj) {
  const pathArray = Array.isArray(dottedPath) ? dottedPath : dottedPath.split('.');

  return pathArray.reduce((acc, pathItem) => acc[pathItem], obj);
}

class CountUniqueValues extends Transform {

  constructor(dottedPath) {

    super({ objectMode: true });

    this.uniqueKeys = {};
    this.path = dottedPath.split('.');
  }

  _transform(feature, enc, next) {

    try {

      const value = getValueInPath(this.path, feature);

      if (!this.uniqueKeys[value]) {
        this.uniqueKeys[value] = 1;
      } else {
        this.uniqueKeys[value]++;
      }
    } catch (err) {
      console.warn('--------------------------------------------------------------------');
      console.warn(feature);
      console.warn(err.stack);
    }

    next();
  }

  _flush(done) {
    this.push(this.uniqueKeys);
    done();
  }
}



/**
 * @param {string} dottedPath - come from `--count-unique-values`
 */
export const countUniqueValues = (dottedPath) => new CountUniqueValues(dottedPath);
