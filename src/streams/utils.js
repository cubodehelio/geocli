import { Transform } from 'stream';
import * as geojsonhint from '@mapbox/geojsonhint';
import bbox from '@turf/bbox';
import uid from 'uid-safe';
import { parsePolygonString } from '../utils';

const jsonify = (obj, pretty) => pretty ? JSON.stringify(obj, null, 2) : JSON.stringify(obj);

class ToFeatureCollectionStream extends Transform {

  constructor() {
    super({ objectMode: true });
    this.startString = '{ "type":"FeatureCollection","features":[';
    this.endString = ']}';
    this.started = false;
  }

  _transform(feature, enc, next) {

    const featureStr = typeof feature === 'string' ? feature : JSON.stringify(feature);
    const str = this.started ? ','.concat(featureStr) : this.startString.concat(featureStr);

    this.started = true;
    this.push(str);
    next();
  }

  _flush(done) {
    this.push(this.endString);
    done();
  }
}


class CollectInvalid extends Transform {

  constructor() {
    super({ objectMode: true });
  }

  _transform(feature, enc, next) {

    const errors = geojsonhint.hint(feature);

    if (errors.length) {
      console.error(errors);
      process.exit();
    }

    next();
  }

  _flush(done) {
    done();
  }
}

/**
 * JSON stringify js
 * @param {Boolean} pretty - prettify the output
 */
export const toJSON = (pretty = false) => new Transform({
  objectMode: true,
  transform(obj, enc, next) {

    const json = jsonify(obj, pretty);

    this.push(json);
    next();
  }
});


export const toFeatureCollection = () => new ToFeatureCollectionStream();

export const collectInvalid = () => new CollectInvalid();

export const addBbox = () => new Transform({
  objectMode: true,
  transform(feature, enc, next) {

    feature.geometry.bbox = bbox(feature);
    this.push(feature);
    next();
  }
});

/**
 * receives a "geo-js" feature, calculates the bbox and return an
 * non-geojson object containing only the original properties plus
 * the bbox property
 */
export const toBbox = () => new Transform({
  objectMode: true,
  transform(feature, enc, next) {
    feature.properties.bbox = bbox(feature);
    this.push(feature.properties);
    next();
  }
});

export const toJSONArray = (pretty = false) => {

  const startString = '[';
  const endString = ']';

  let started = false;

  return new Transform({
    objectMode: true,
    transform(item, enc, next) {

      const jsonItem = typeof item === 'string' ? item : jsonify(item, pretty);
      const str = started ? ','.concat(jsonItem) : startString.concat(jsonItem);

      started = true;
      this.push(str);
      next();
    },
    flush(done) {
      this.push(endString);
      done();
    }
  });
};


export const toDebug = new Transform({
  transform(item, enc, next) {
    let foo;

    if (Buffer.isBuffer(item)) {
      foo = item.toString();
    }

    console.log(foo);
    process.exit();
    next();
  }
});


export const removeProperties = (props = []) => new Transform({
  objectMode: true,
  transform(obj, enc, next) {

    props.forEach(key => {
      delete obj.properties[key];
    });

    this.push(obj);
    next();
  }
});


export const addUid = () => new Transform({
  objectMode: true,
  transform(feature, enc, next) {

    feature.properties.uid = uid.sync(18);
    this.push(feature);
    next();
  }
});


/**
 * show the first line of a string and exit
 */
export const firstLineAndExit = () => new Transform({
  objectMode: false,
  transform(chunk, enc, next) {

    const str = chunk.toString();
    const linesArray = str.split('\n')[0].replace(/"/g, '').split(',');

    console.log(linesArray);
    process.exit();
  }
});

/**
 * in JSON str
 * out JS
 */
export const jsonStringToJs = new Transform({
  objectMode: true,
  transform(chunk, enc, next) {

    const json = chunk.toString();
    const js = JSON.parse(json);

    this.push(js);
    next();
  }
});


/**
 * in: JS
 * out: JS
 *
 * @param {*} renames
 */
export const renameKeys = renames => new Transform({
  objectMode: true,
  transform(obj, enc, next) {

    const newObj = Object.keys(obj).reduce((acc, originalKey) => {

      if (renames[originalKey]) {
        acc[renames[originalKey]] = obj[originalKey];
      } else {
        acc[originalKey] = obj[originalKey];
      }

      return acc;
    }, {});

    this.push(newObj);
    next();
  }
});


export const parseEmbeddedJson = keyName => new Transform({
  objectMode: true,
  transform(obj, enc, next) {

    if (typeof obj[keyName] !== 'undefined') {
      obj[keyName] = JSON.parse(obj[keyName]);
    } else {
      console.warn(`Warning: key name "${keyName}" is not defined`);
    }

    this.push(obj);
    next();
  }
});

/**
 * convert a regular object having a "geometry" key into
 * a geojson feature object
 *
 * IN: JS
 */
export const toFeature = new Transform({
  objectMode: true,
  transform(obj, enc, next) {

    const feature = {
      type: 'Feature',
      geometry: {},
      properties: {}
    };

    if (!obj.geometry) {

      console.warn('Warning: item does not have a "geometry" key');

    } else if (typeof obj.geometry === 'object') {

      feature.properties = obj;
      feature.geometry = Object.assign({}, obj.geometry);
      delete obj.geometry;


    } else if (typeof obj.geometry === 'string') {

      const polygon = parsePolygonString(obj.geometry);

      feature.geometry = {
        type: 'Polygon',
        coordinates: [polygon]
      };

      delete obj.geometry;
      feature.properties = obj;

    } else {
      console.warn('Error: `geometry` property does not have the proper format:');
      console.warn();
      console.warn(obj.geometry);
      process.exit()
    }

    this.push(feature);
    next();
  }
});
