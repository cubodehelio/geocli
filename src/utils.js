/**
 * Covert a string like:
 *
 * ```
 * "-68.09599718832222,-38.94101958422372 -68.09567643840339,-38.94061805516469 ..."
 * ```
 * to an array like:
 *
 * ```js
 * [
 *   [ -68.09599718832222, -38.94101958422372 ],
 *   [ -68.09567643840339, -38.94061805516469 ],
 *   [ ... ]
 * ]
 * ```
 *
 * @param {String} str
 * @return {Array<Array>} array of tuples of float values
 */
export const parsePolygonString = str => str.split(' ').map(pair => pair.split(',').map(coordString => parseFloat(coordString)));
