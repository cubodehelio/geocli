import { createWriteStream, createReadStream } from 'fs';
import { resolve } from 'path';
import csv from 'csvtojson';
import * as utils from '../streams/utils';

export default config => new Promise((res, reject) => {

  let destinationStream;

  if (config.dest) {
    destinationStream = createWriteStream(resolve(process.cwd(), config.dest));

  } else {
    destinationStream = process.stdout;
  }

  /**
   * create the readable stream
   */

  const csvReadableSrc = createReadStream(config.src);

  csvReadableSrc.on('error', err => {

    if (err.message.match(/ENOENT/g)) {
      reject(err);
    }

    console.warn(err);
  });

  if (config.showColumns) {

    // this will exit after console.log the first line
    csvReadableSrc.pipe(utils.firstLineAndExit());
  }

  let stream = csv().
    fromStream(csvReadableSrc)
    .pipe(utils.jsonStringToJs);

  if (config.renameColumns) {
    stream = stream.pipe(utils.renameKeys(config.renameColumns));
  }

  if (config.parseEmbeddedJson) {
    stream = stream.pipe(utils.parseEmbeddedJson(config.parseEmbeddedJson));
  }

  if (config.toFeature) {
    stream = stream.pipe(utils.toFeature);
  }

  /**
   * convert to json
   */
  stream = stream.pipe(utils.toJSON(config.pretty));

  if (config.toFeatureCollection) {
    stream = stream.pipe(utils.toFeatureCollection());
  } else {
    stream = stream.pipe(utils.toJSONArray(config.pretty));
  }

  stream.pipe(destinationStream);
});
