import { Transform } from 'stream';

export default class ToFeatureCollectionStream extends Transform {

  constructor() {
    super({ objectMode: true });
    this.startString = '{ "type":"FeatureCollection","features":[';
    this.endString = ']}';
    this.started = false;
  }

  _transform(feature, enc, next) {

    const str = this.started
      ? ','.concat(JSON.stringify(feature))
      : this.startString.concat(JSON.stringify(feature));

    this.started = true;

    this.push(str);
    next();

  }

  _flush(done) {
    this.push(this.endString);
    done();
  }
}
