#!/usr/bin/env bash
reset; csvtogeo barrios.csv barrios.json \
  --rename-columns="barrio_nombre:FNA,localidad_comuna_nombre:CMU,partido_departamento_nombre:DTO,provincia_nombre:PROV,geojson:geometry" \
  --parse-embedded-json="geometry" \
  --to-feature \
  --to-feature-collection
