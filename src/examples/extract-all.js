const readdirSync = require('fs').readdirSync;
const execSync = require('child_process').execSync;

/**
 * run this example: `node extract-all.js`
 */

const src = '/home/jcanabate/geo/downloads/drive/intervenciones-v3/';

readdirSync(src).filter(file => file.match(/\.zip$/gi)).forEach(file => {

  const filename = src.concat(file);
  const newFilename = src.concat(file.replace('.zip', '.geojson'));

  execSync(`zip2geojson ${filename} ${newFilename} -d`);

  console.log('done:', file);
});


console.log('all done');
