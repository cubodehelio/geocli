/* eslint global-require: 0 */

import { createWriteStream } from 'fs';
import { resolve } from 'path';
import zip2GeojsonStream from './zip2GeojsonStream';
import * as analytics from './streams/analytics';
import * as utils from './streams/utils';

export default (config) => {

  const zipSrc = resolve(process.cwd(), config.src);

  let destStream;

  if (config.dest) {
    destStream = createWriteStream(resolve(process.cwd(), config.dest));
  } else {
    destStream = process.stdout;
  }


  return zip2GeojsonStream(zipSrc).then((readStream) => {

    let stream = readStream;

    if (config.countUniqueValues) {

      stream = stream.
        pipe(analytics.countUniqueValues(config.countUniqueValues)).
        pipe(utils.toJSON(config.pretty));

    } else if (config.autouid) {

      stream = stream.
        pipe(utils.addUid()).
        pipe(utils.toJSON(config.pretty)).
        pipe(utils.toFeatureCollection());

    } else if (config.collectInvalid) {

      console.error('--collect-invalid TODO!');
      process.exit(1);

      stream = stream.
        pipe(utils.collectInvalid()).
        pipe(utils.toJSON(config.pretty));

    } else {

      stream = stream.pipe(utils.toFeatureCollection());

    }

    stream.pipe(destStream);

    return new Promise((res, reject) => {

      stream.on('error', reject);
      stream.on('close', () => {

        if (config.dest) {
          console.log('stream close');
        }

        return res();
      });
    });
  });
};
