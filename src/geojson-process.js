import { createWriteStream, createReadStream } from 'fs';
import { resolve } from 'path';
import * as utils from './streams/utils';
import geojsonStream from 'geojson-stream';

export default config => new Promise((res, reject) => {

  const jsonSrc = resolve(process.cwd(), config.src);

  let destStream;

  if (config.dest) {
    destStream = createWriteStream(resolve(process.cwd(), config.dest));
  } else {
    destStream = process.stdout;
  }

  const readable = createReadStream(jsonSrc);

  let pipe = readable.pipe(geojsonStream.parse());

  if (config.autouid) {
    pipe = pipe.pipe(utils.addUid());
  }

  if (config.removeProperties && config.removeProperties.length) {
    pipe = pipe.pipe(utils.removeProperties(config.removeProperties));
  }

  if (config.addBbox) {
    pipe = pipe.pipe(utils.addBbox());
  }

  /**
   * basically we need to handle two possible pipes ona that at the end leads
   * in a plain object that need to be converted to json string
   * and other that is already a json string.
   */

  if (config.toBbox) {
    pipe = pipe.
      pipe(utils.toBbox()).
      pipe(utils.toJSONArray(config.pretty));
  } else {
    pipe = pipe.pipe(utils.toFeatureCollection());
  }

  pipe = pipe.pipe(destStream);

  pipe.on('error', (err) => {
    // console.error('stream error...')
    reject(err);
  });
  pipe.on('close', () => {

    if (config.dest) {
      console.log('stream close');
    }

    return res();
  });
});
