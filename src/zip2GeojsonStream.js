import { Open } from 'unzipper';

/**
 * @param {String} zipfilePath - path to the zip file
 * @return {Promise} - resolves to central directory
 * information object with methods to extract individual
 * files.
 */
const filterSources = sources => sources.files.filter(info => info.path.match(/\.(dbf|shp)$/));
const getShpSources = sources => sources.filter(source => source.path.match(/\.(shp)$/));

import { Readable } from 'stream';

const shapefile = require('shapefile'); // undefined when use `import`

class ShapeReadStream extends Readable {

  constructor(shpStream, dbfStream) {

    super({ objectMode: true });

    this.shpStream = shpStream;
    this.dbfStream = dbfStream;
    this.source = null;
  }

  async readStream() {

    if (!this.source) {
      this.source = await shapefile.open(this.shpStream, this.dbfStream, {
        encoding: 'utf-8'
      });
    }

    return await this.source.read();
  }

  _read() {

    this.readStream().then((result) => {
      this.push(result.done ? null : result.value);
    });

  }
}

export default (path, options) => Open.file(path).then(sources => {

  const shpAndDbfInfo = filterSources(sources);

  const shpSources = getShpSources(shpAndDbfInfo);

  if (!shpSources.length) {
    throw new Error(`The zip file ${path} does not contains any .shp file`);
  } else if (shpSources.length > 1) {
    throw new Error(`The zip file ${path} contains more than ONE .shp file`);
  }

  const shpPath = shpSources[0].path;
  const dbfSource = shpAndDbfInfo.find(source => source.path === shpPath.replace(/\.(shp)/, '.dbf'));

  if (!dbfSource) {
    throw new Error(`the passed zip file ${path} does not contains any ".dbf" file`);
  }

  const shpStream = shpSources[0].stream;
  const dbfStream = dbfSource.stream;

  return new ShapeReadStream(shpStream(), dbfStream());

});