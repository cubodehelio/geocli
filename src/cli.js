#!/usr/bin/env node

/**
 * Module dependencies.
 */

import program from 'commander';
import zip2geojson from './zip2geojson';

let srcPath, destPath;

program.
  version(require('../package.json').version).
  option('-u, --count-unique-values <dottedPath>').
  option('-p, --pretty [boolean]').
  option('-i, --collect-invalid [boolean]').
  option('-b, --tobbox [boolean]').
  option('-d, --autouid [boolean]', 'generate and add a UID in every geojson feature').
  usage(`

  $ ziptogeo <src>               # Output go directly to stdout.
  $ ziptogeo <src> [dest]        # destination json file path (optional).
  $ ziptogeo <src> > dest.json   # pipe the output to a file.
  `).
  arguments('<src> [dest]').action((src, dest) => {
    srcPath = src;
    destPath = dest;
  }).
  parse(process.argv);


if (!srcPath) {

  program.help();

} else {

  const cfg = {
    src: srcPath,
    dest: destPath,
    countUniqueValues: program.countUniqueValues,
    pretty: program.pretty,
    collectInvalid: program.collectInvalid,
    tobbox: program.tobbox,
    autouid: program.autouid
  };

  zip2geojson(cfg).then(() => {
    process.exit();
  }).catch(err => {
    console.error(err.stack);
    process.exit(1);
  });

}
